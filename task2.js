/*
Task 2 - Supper array of object
Note: Don't use any third party library***
    ### Input
    const aob = 
    [
        { framework: 'React.JS', website: 'Paypal' },
        { framework: 'React.JS', website: 'Tesla' },
        { framework: 'Angular', website: 'Google' },
        { framework: 'Next.js', website: 'holabhavin' },
        { framework: 'Vue.JS', website: 'Vue' },
        { framework: 'JavaScript', website: 'heynikhil' },
    ]
    

    ### Output
    [
        { victim: 'React.JS', count: 2 },
        { victim: 'Angular', count: 1 },
        { victim: 'Vue.JS', count: 1 },
    	{ victim: 'Next.js', count: 1 },
        { victim: 'JavaScript', count: 1 }
    ]
*/
const ip = [
    { framework: 'React.JS', website: 'Paypal' },
    { framework: 'React.JS', website: 'Tesla' },
    { framework: 'Angular', website: 'Google' },
    { framework: 'Next.js', website: 'holabhavin' },
    { framework: 'Vue.JS', website: 'Vue' },
    { framework: 'JavaScript', website: 'heynikhil' },
];

const temp = {}, op = [];

for (obj of ip)
    temp[obj.framework] = temp[obj.framework] > 0 ? temp[obj.framework] + 1 : 1;

for (const [key, value] of Object.entries(temp))
    op.push({ victim: key, count: value });


console.log(op)
