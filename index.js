require("./env");

const { mongodb } = require("./utils");
const { User } = require("./model");
const app = require("express")();
mongodb.initialize();

function randomInt(min, max) {
  return Math.round(min + Math.random() * (max - min));
}

app.get("/insert/:value", async (req, res) => {
  const msg = `inserting ${req.params.value} document to DB!!!\nplease wait...`;
  const aUsers = [];
  await User.create({ sName: "anuj", nAge: 22, nXp: 500, nLevel: 5 });
  for (let i = 2; i <= req.params.value; i++) {
    aUsers.push({ sName: `user${i}`, nAge: randomInt(18, 65), nXp: randomInt(0, 800), nLevel: randomInt(1, 9) });
    if (!(i % 50)) {
      await User.insertMany(aUsers);
      aUsers.length = 0;
    }
  }
  res.send(msg);
});

app.get("/getall", async (req, res) => {
    const aUsers = await User.find({});
    res.json(aUsers);
});

//http://localhost:4000/leaderboard/60cb7101163e15f02ef766db
app.get("/leaderboard/:_id", async (req, res) => {
  var pipeline = [
    { $project: { _id : 1, sName: 1, nAge: 1, nXp: 1, nLevel: 1, nScore: { $divide: ['$nXp', '$nLevel'], } } },
    { 
      $group: {
          _id: "$nScore",
          data: {$push: "$$ROOT"},
      }
    },
    { $sort: { _id: -1 } },
    { 
      $group: {
          _id: null,
          data: {$push: "$$ROOT"},
      }
    },
    { $unwind: { "path": "$data", "includeArrayIndex": "data.nRank" } },
    { $unwind: { "path": "$data.data" } },
    { $project: {
        _id: "$data.data._id",
        "sName":  "$data.data.sName",
        "nAge" : "$data.data.nAge",
        "nXp" : "$data.data.nXp",
        "nLevel" : "$data.data.nLevel",
        "nScore" : "$data.data.nScore",
        "nRank": {$add: ["$data.nRank", 1]}
    }},
    {
        $group: {
            _id: null,
            inputData: { $push: "$$ROOT" },
        }
    },
    {
        $addFields: {
            leaderboard: {
                $reduce: {
                    input: '$inputData',
                    initialValue: { top9: [], me: [] },
                    in: {
                        "top9": {
                            $concatArrays: [
                                '$$value.top9',
                                {
                                    $cond: [
                                        {
                                            $lt: ['$$this.nRank', 10],
                                        },
                                        ["$$this"],
                                        [],
                                    ],
                                }
                            ]
                        },
                        "me": {
                            $concatArrays: [
                                '$$value.me',
                                {
                                    $cond: [
                                        {
                                            $eq: ['$$this._id', mongodb.mongify(req.params._id)],
                                        },
                                        ["$$this"],
                                        [],
                                    ],
                                }
                            ]
                        },
                    }
                }
            }
        }
    },
    { $project: { _id: false, leaderboard: { $concatArrays: ["$leaderboard.top9", "$leaderboard.me"] } } },
    { $unwind: { "path": "$leaderboard" }},
    { $replaceRoot: { newRoot: "$leaderboard" } }

  ];

  await User.aggregate(pipeline, (error, aUsers)=>{
    if(!error) return res.json(aUsers);
    return res.json('error')
  });
});

//http://localhost:4000/leaderboard2/60cb7101163e15f02ef766db
app.get("/leaderboard2/:_id", async (req, res) => {
  const pipeline = [
    { $project: { _id: 1, sName: 1, nAge: 1, nXp: 1, nLevel: 1, nScore: { $divide: ['$nXp', '$nLevel'], } } },
    { $sort: { nScore: -1 } },
    {
        $group: {
            _id: null,
            data: { $push: "$$ROOT" },
        }
    },
    { $unwind: { "path": "$data", "includeArrayIndex": "data.nRank" } },
    {
        $project: {
            _id: "$data._id",
            "sName": "$data.sName",
            "nAge": "$data.nAge",
            "nXp": "$data.nXp",
            "nLevel": "$data.nLevel",
            "nScore": "$data.nScore",
            "nRank": { $add: ["$data.nRank", 1] }
        }
    },
    {
        $group: {
            _id: null,
            inputData: { $push: "$$ROOT" },
        }
    },
    {
        $addFields: {
            leaderboard: {
                $reduce: {
                    input: '$inputData',
                    initialValue: { top9: [], me: [] },
                    in: {
                        "top9": {
                            $concatArrays: [
                                '$$value.top9',
                                {
                                    $cond: [
                                        {
                                            $lt: ['$$this.nRank', 10],
                                        },
                                        ["$$this"],
                                        [],
                                    ],
                                }
                            ]
                        },
                        "me": {
                            $concatArrays: [
                                '$$value.me',
                                {
                                    $cond: [
                                        {
                                            $eq: ['$$this._id', mongodb.mongify(req.params._id)],
                                        },
                                        ["$$this"],
                                        [],
                                    ],
                                }
                            ]
                        },
                    }
                }
            }
        }
    },
    { $project: { _id: false, leaderboard: { $concatArrays: ["$leaderboard.top9", "$leaderboard.me"] } } },
    { $unwind: { "path": "$leaderboard" }},
    { $replaceRoot: { newRoot: "$leaderboard" } }
  ]

  await User.aggregate(pipeline, (error, aUsers)=>{
    if(!error) return res.json(aUsers);
    return res.json('error')
  });
  
});

app.listen(process.env.PORT, () => {
  console.log(`Running @ http://localhost:${process.env.PORT}`);
});
