const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema(
    {
        sName: { type: String, default: '' },
        nAge: { type: Number, default: 18, min: 18, max: 65 },
        nXp: { type: Number, default: 0,  min: 0, max: 800 },
        nLevel: { type: Number, default: 1, min: 1, max: 9 },
        
    },
    { timestamps: false }
);

module.exports = mongoose.model('User', UserSchema, 'User');
